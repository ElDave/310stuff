SRCS += \
	$(EXTERN_EXERCISE_PATH)/app_skeleton_shadow.cpp \
	$(EXTERN_COMMON_PATH)/rendSkeleton.cpp \
	$(EXTERN_COMMON_PATH)/rendIndexedTrilist.cpp \
	$(EXTERN_COMMON_PATH)/util_tex.cpp \
	$(EXTERN_COMMON_PATH)/util_file.cpp \
	$(EXTERN_COMMON_PATH)/util_misc.cpp

CFLAGS += \
	-Wno-maybe-uninitialized
