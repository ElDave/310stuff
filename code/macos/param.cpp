#include <unistd.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include "param.h"
#include "stream.hpp"

// verify iostream-free status
#if _GLIBCXX_IOSTREAM
#error rogue iostream acquired
#endif

namespace stream {

// deferred initialization by main()
in cin;
out cout;
out cerr;

} // namespace stream

Param param;
extern "C" unsigned input;
unsigned input;

// external app-specific cli parser
int parse_cli(
    const unsigned argc,
    const char* const* argv);

namespace { // anonymous

const char arg_prefix[]     = "-";
const char arg_screen[]     = "screen";
const char arg_bitness[]    = "bitness";
const char arg_fsaa[]       = "fsaa";
const char arg_nframes[]    = "frames";
const char arg_app[]        = "app";

bool validate_fullscreen(
	const char* string_w,
	const char* string_h,
	const char* string_hz,
	unsigned &screen_w,
	unsigned &screen_h)
{
	if (NULL == string_w || NULL == string_h || NULL == string_hz)
		return false;

	unsigned w, h, hz;

	if (1 != sscanf(string_w, "%u", &w))
		return false;

	if (1 != sscanf(string_h, "%u", &h))
		return false;

	if (1 != sscanf(string_hz, "%u", &hz))
		return false;

	if (!w || !h || !hz)
		return false;

	screen_w = w;
	screen_h = h;

	return true;
}

bool validate_bitness(
	const char* string_r,
	const char* string_g,
	const char* string_b,
	const char* string_a,
	unsigned (&screen_bitness)[4])
{
	if (NULL == string_r || NULL == string_g || NULL == string_b || NULL == string_a)
		return false;

	unsigned bitness[4];

	if (1 != sscanf(string_r, "%u", &bitness[0]))
		return false;

	if (1 != sscanf(string_g, "%u", &bitness[1]))
		return false;

	if (1 != sscanf(string_b, "%u", &bitness[2]))
		return false;

	if (1 != sscanf(string_a, "%u", &bitness[3]))
		return false;

	if (!bitness[0] || 16 < bitness[0] ||
		!bitness[1] || 16 < bitness[1] ||
		!bitness[2] || 16 < bitness[2] ||
		16 < bitness[3])
	{
		return false;
	}

	screen_bitness[0] = bitness[0];
	screen_bitness[1] = bitness[1];
	screen_bitness[2] = bitness[2];
	screen_bitness[3] = bitness[3];

	return true;
}

} // namespace

int parseCLI(
	const int argc,
	char** const argv,
	Param* param)
{
	assert(param);

	bool cli_err = false;
	bool cli_app = false;

	const size_t prefix_len = strlen(arg_prefix);

	for (int i = 1; i < argc && !cli_err; ++i) {
		if (strncmp(argv[i], arg_prefix, prefix_len)) {
			cli_err = !cli_app;
			continue;
		}

		if (!strcmp(argv[i] + prefix_len, arg_app)) {
			if (++i == argc)
				cli_err = true;

			cli_app = true;
			continue;
		}

		cli_app = false;

		if (i + 1 < argc && !strcmp(argv[i] + prefix_len, arg_nframes)) {
			if (1 != sscanf(argv[i + 1], "%u", &param->frames))
				cli_err = true;

			i += 1;
			continue;
		}

		if (i + 3 < argc && !strcmp(argv[i] + prefix_len, arg_screen)) {
			if (!validate_fullscreen(argv[i + 1], argv[i + 2], argv[i + 3], param->image_w, param->image_h))
				cli_err = true;

			i += 3;
			continue;
		}

		if (i + 4 < argc && !strcmp(argv[i] + prefix_len, arg_bitness)) {
			if (!validate_bitness(argv[i + 1], argv[i + 2], argv[i + 3], argv[i + 4], param->bitness))
				cli_err = true;

			i += 4;
			continue;
		}

		if (i + 1 < argc && !strcmp(argv[i] + prefix_len, arg_fsaa)) {
			if (1 != sscanf(argv[i + 1], "%u", &param->fsaa))
				cli_err = true;

			i += 1;
			continue;
		}

		cli_err = true;
	}

	if (cli_err) {
		stream::cerr << "usage: " << argv[0] << " [<option> ...]\n"
			"options:\n"
			"\t" << arg_prefix << arg_screen << " <width> <height> <Hz>\t\t: set screen output of specified geometry and refresh\n"
			"\t" << arg_prefix << arg_bitness << " <r> <g> <b> <a>\t\t: set GL config of specified RGBA bitness; default is screen's bitness\n"
			"\t" << arg_prefix << arg_fsaa << " <positive_integer>\t\t: set GL fullscreen antialiasing; default is none\n"
			"\t" << arg_prefix << arg_nframes << " <unsigned_integer>\t\t: set number of frames to run; default is max unsigned int\n"
			"\t" << arg_prefix << arg_app << " <option> [<arguments>]\t\t: app-specific option\n";

		return 1;
	}

	// fall through to app-specific cli parser
	return parse_cli(argc, argv);
}

