#!/bin/bash

shopt -s nullglob

if [ $# -lt 1 ] || [ $# -gt 2 ] || [ ! -d ../exercise_${1} ]; then
	echo usage: ${0} '<exercise_index> [clean]'
	exit 1
fi

EXTERN_EXERCISE_PATH=../exercise_${1}
EXTERN_COMMON_PATH=../common
ARTIFACT_PATH=exercise.app/Contents/MacOS

if [[ $2 == "clean" ]]; then
	HOSTTYPE=${HOSTTYPE} EXTERN_COMMON_PATH=${EXTERN_COMMON_PATH} EXTERN_EXERCISE_PATH=${EXTERN_EXERCISE_PATH} make clean

	if [ -d ${ARTIFACT_PATH} ]; then
		FILES=(${ARTIFACT_PATH}/{*.glsl?,*.raw,*.mesh,*.skeleton})

		if [[ ${#FILES[@]} -ne 0 ]]; then
			rm ${FILES[@]}
		fi
		rmdir ${ARTIFACT_PATH}
	fi

	exit 0
fi

if [ ! -d ${ARTIFACT_PATH} ]; then
	mkdir ${ARTIFACT_PATH}
fi

HOSTTYPE=${HOSTTYPE} EXTERN_COMMON_PATH=${EXTERN_COMMON_PATH} EXTERN_EXERCISE_PATH=${EXTERN_EXERCISE_PATH} make

if (( $? == 0 )); then
	FILES=(${EXTERN_EXERCISE_PATH}/{*.glsl?,*.raw,*.mesh,*.skeleton})

	if [[ ${#FILES[@]} -ne 0 ]]; then
		cp ${FILES[@]} ${ARTIFACT_PATH}
	fi
fi
