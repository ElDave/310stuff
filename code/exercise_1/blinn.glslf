///essl #version 100
///glsl #version 150

////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Lambert & Blinn, fragment shader
////////////////////////////////////////////////////////////////////////////////////////////////////////////

#if defined(GL_ES)
#extension GL_OES_standard_derivatives : require

#if defined(GL_FRAGMENT_PRECISION_HIGH)
	precision highp float;
#else
	precision mediump float;
#endif

#define in_qualifier varying
#define xx_FragColor gl_FragColor

#else

#define in_qualifier in
out vec4 xx_FragColor;

#endif

const vec4 albedo           = vec4(1.0);
const vec4 scene_ambient    = vec4(0.2, 0.2, 0.2, 1.0);
const vec3 lprod_diffuse    = vec3(0.5, 0.5, 0.5);
const vec3 lprod_specular   = vec3(0.7, 0.7, 0.5);
const float shininess       = 64.0;

in_qualifier vec3 p_obj_i; // vertex position in object space
in_qualifier vec3 l_obj_i; // light_source vector in object space
in_qualifier vec3 h_obj_i; // half-direction vector in object space

void main()
{
	vec3 p_dx = dFdx(p_obj_i);
	vec3 p_dy = dFdy(p_obj_i);

	vec3 n = normalize(cross(p_dx, p_dy));

	vec3 l_obj = normalize(l_obj_i);
	vec3 h_obj = normalize(h_obj_i);

	vec3 d = lprod_diffuse *      max(dot(l_obj, n), 0.0);
	vec3 s = lprod_specular * pow(max(dot(h_obj, n), 0.0), shininess);

	// be considerate of the older SIMD architectures and compilers
	xx_FragColor =
		vec4(s, 0.0) +
		vec4(d, 0.0) * albedo +
		scene_ambient * albedo;
}
