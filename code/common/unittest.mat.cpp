#include <iomanip>
#include "timer.h"
#include "stream.hpp"
#include "vectnative.hpp"

#ifdef _WIN32
#include <SDKDDKVer.h>
#include <stdio.h>

#else
// verify iostream-free status (see further down)
#if _GLIBCXX_IOSTREAM != 0
#error rogue iostream acquired
#endif

#endif
// use a custom minimalistic implementation of std::istream/ostream to avoid the highly-harmful-for-the-optimisations
// static initialization of std::cin/cout/cerr; our mini-implementation is initialized at the start of main()
namespace stream {
in cin;
out cout;
out cerr;
}

#if _MSC_VER != 0
using namespace simd; // oldest msvc we target does not support using-declarations, so we go full monty there
#else
#if NATIVE_F64X4 != 0
using simd::f64x4;
using simd::as_f64x4;
using simd::get_f64x2;
#endif
#if NATIVE_S64X4 != 0
using simd::s64x4;
using simd::as_s64x4;
using simd::get_s64x2;
#endif
#if NATIVE_U64X4 != 0
using simd::u64x4;
using simd::as_u64x4;
using simd::get_u64x2;
#endif
#if NATIVE_F32X8 != 0
using simd::f32x8;
using simd::as_f32x8;
using simd::get_f32x4;
#endif
#if NATIVE_S32X8 != 0
using simd::s32x8;
using simd::as_s32x8;
using simd::get_s32x4;
#endif
#if NATIVE_U32X8 != 0
using simd::u32x8;
using simd::as_u32x8;
using simd::get_u32x4;
#endif
#if NATIVE_F32X2 != 0
using simd::f32x2;
using simd::as_f32x2;
#endif
#if NATIVE_S32X2 != 0
using simd::s32x2;
using simd::as_s32x2;
#endif
#if NATIVE_U32X2 != 0
using simd::u32x2;
using simd::as_u32x2;
#endif
#if NATIVE_S16X16 != 0
using simd::s16x16;
using simd::as_s16x16;
using simd::get_s16x8;
#endif
#if NATIVE_U16X16 != 0
using simd::u16x16;
using simd::as_u16x16;
using simd::get_u16x8;
#endif
#if NATIVE_S16X4 != 0
using simd::s16x4;
using simd::as_s16x4;
#endif
#if NATIVE_U16X4 != 0
using simd::u16x4;
using simd::as_u16x4;
#endif
using simd::f64x2;
using simd::s64x2;
using simd::u64x2;
using simd::f32x4;
using simd::s32x4;
using simd::u32x4;
using simd::s16x8;
using simd::u16x8;
using simd::as_f64x2;
using simd::as_s64x2;
using simd::as_u64x2;
using simd::as_f32x4;
using simd::as_s32x4;
using simd::as_u32x4;
using simd::as_s16x8;
using simd::as_u16x8;
using simd::all;
using simd::any;
using simd::none;
using simd::min;
using simd::max;
using simd::abs;
using simd::mask;
using simd::select;
using simd::sqrt;
using simd::log;
using simd::exp;
using simd::pow;
using simd::sin;
using simd::cos;
using simd::sincos;
using simd::transpose4x4;
using simd::operator ==;
using simd::operator !=;
using simd::operator <;
using simd::operator >;
using simd::operator <=;
using simd::operator >=;
using simd::flag_zero;
using simd::flag_native;
#endif

template < typename T >
const T& min(const T& a, const T& b) {
	return (a < b) ? a : b;
}

template < typename T >
const T& max(const T& a, const T& b) {
	return (a > b) ? a : b;
}

class formatter_f64 {
	double f;

public:
	formatter_f64(const double a) : f(a) {}

	uint64_t get() const {
		return reinterpret_cast< const uint64_t& >(f);
	}
};

class formatter_f32 {
	float f;

public:
	formatter_f32(const float a) : f(a) {}

	uint32_t get() const {
		return reinterpret_cast< const uint32_t& >(f);
	}
};

static stream::out& operator << (
	stream::out& str,
	const formatter_f64 a) {

	str << 
		(a.get() >> 63) << ':' <<
		std::setw(3)  << std::setfill('0') << (a.get() >> 52 & 0x7ff) << ':' <<
		std::setw(13) << std::setfill('0') << (a.get() >>  0 & 0xfffffffffffff);
	return str;
}

static stream::out& operator << (
	stream::out& str,
	const formatter_f32 a) {

	str << 
		(a.get() >> 31) << ':' <<
		std::setw(2) << std::setfill('0') << (a.get() >> 23 & 0xff) << ':' <<
		std::setw(6) << std::setfill('0') << (a.get() >>  0 & 0x7fffff);
	return str;
}

#if NATIVE_F64X4 != 0
static stream::out& operator << (
	stream::out& str,
	const f64x4& a) {

	return str <<
		formatter_f64(a[0]) << ' ' << formatter_f64(a[1]) << ' ' << formatter_f64(a[2]) << ' ' << formatter_f64(a[3]);
}

#endif
#if NATIVE_S64X4 != 0
static stream::out& operator << (
	stream::out& str,
	const s64x4& a) {

	return str <<
		a[0] << ' ' << a[1] << ' ' << a[2] << ' ' << a[3];
}

#endif
#if NATIVE_U64X4 != 0
static stream::out& operator << (
	stream::out& str,
	const u64x4& a) {

	return str <<
		a[0] << ' ' << a[1] << ' ' << a[2] << ' ' << a[3];
}

#endif
static stream::out& operator << (
	stream::out& str,
	const f64x2& a) {

	return str <<
		formatter_f64(a[0]) << ' ' << formatter_f64(a[1]);
}

static stream::out& operator << (
	stream::out& str,
	const s64x2& a) {

	return str <<
		a[0] << ' ' << a[1];
}

static stream::out& operator << (
	stream::out& str,
	const u64x2& a) {

	return str <<
		a[0] << ' ' << a[1];
}

#if NATIVE_F32X8 != 0
static stream::out& operator << (
	stream::out& str,
	const f32x8& a) {

	return str <<
		formatter_f32(a[0]) << ' ' << formatter_f32(a[1]) << ' ' << formatter_f32(a[2]) << ' ' << formatter_f32(a[3]) << ' ' <<
		formatter_f32(a[4]) << ' ' << formatter_f32(a[5]) << ' ' << formatter_f32(a[6]) << ' ' << formatter_f32(a[7]);
}

#endif
#if NATIVE_S32X8 != 0
static stream::out& operator << (
	stream::out& str,
	const s32x8& a) {

	return str <<
		a[0] << ' ' << a[1] << ' ' << a[2] << ' ' << a[3] << ' ' <<
		a[4] << ' ' << a[5] << ' ' << a[6] << ' ' << a[7];
}

#endif
#if NATIVE_U32X8 != 0
static stream::out& operator << (
	stream::out& str,
	const u32x8& a) {

	return str <<
		a[0] << ' ' << a[1] << ' ' << a[2] << ' ' << a[3] << ' ' <<
		a[4] << ' ' << a[5] << ' ' << a[6] << ' ' << a[7];
}

#endif
static stream::out& operator << (
	stream::out& str,
	const f32x4& a) {

	return str <<
		formatter_f32(a[0]) << ' ' << formatter_f32(a[1]) << ' ' << formatter_f32(a[2]) << ' ' << formatter_f32(a[3]);
}

static stream::out& operator << (
	stream::out& str,
	const s32x4& a) {

	return str <<
		a[0] << ' ' << a[1] << ' ' << a[2] << ' ' << a[3];
}

static stream::out& operator << (
	stream::out& str,
	const u32x4& a) {

	return str <<
		a[0] << ' ' << a[1] << ' ' << a[2] << ' ' << a[3];
}

#if NATIVE_F32X2 != 0
static stream::out& operator << (
	stream::out& str,
	const f32x2& a) {

	return str <<
		formatter_f32(a[0]) << ' ' << formatter_f32(a[1]);
}

#endif
#if NATIVE_S32X2 != 0
static stream::out& operator << (
	stream::out& str,
	const s32x2& a) {

	return str <<
		a[0] << ' ' << a[1];
}

#endif
#if NATIVE_U32X2 != 0
static stream::out& operator << (
	stream::out& str,
	const u32x2& a) {

	return str <<
		a[0] << ' ' << a[1];
}

#endif
#if NATIVE_S16X16 != 0
static stream::out& operator << (
	stream::out& str,
	const s16x16& a) {

	return str <<
		a[ 0] << ' ' << a[ 1] << ' ' << a[ 2] << ' ' << a[ 3] << ' ' <<
		a[ 4] << ' ' << a[ 5] << ' ' << a[ 6] << ' ' << a[ 7] << ' ' <<
		a[ 8] << ' ' << a[ 9] << ' ' << a[10] << ' ' << a[11] << ' ' <<
		a[12] << ' ' << a[13] << ' ' << a[14] << ' ' << a[15];
}

#endif
#if NATIVE_U16X16 != 0
static stream::out& operator << (
	stream::out& str,
	const u16x16& a) {

	return str <<
		a[ 0] << ' ' << a[ 1] << ' ' << a[ 2] << ' ' << a[ 3] << ' ' <<
		a[ 4] << ' ' << a[ 5] << ' ' << a[ 6] << ' ' << a[ 7] << ' ' <<
		a[ 8] << ' ' << a[ 9] << ' ' << a[10] << ' ' << a[11] << ' ' <<
		a[12] << ' ' << a[13] << ' ' << a[14] << ' ' << a[15];
}

#endif
static stream::out& operator << (
	stream::out& str,
	const s16x8& a) {

	return str <<
		a[0] << ' ' << a[1] << ' ' << a[2] << ' ' << a[3] << ' ' <<
		a[4] << ' ' << a[5] << ' ' << a[6] << ' ' << a[7];
}

static stream::out& operator << (
	stream::out& str,
	const u16x8& a) {

	return str <<
		a[0] << ' ' << a[1] << ' ' << a[2] << ' ' << a[3] << ' ' <<
		a[4] << ' ' << a[5] << ' ' << a[6] << ' ' << a[7];
}

#if NATIVE_S16X4 != 0
static stream::out& operator << (
	stream::out& str,
	const s16x4& a) {

	return str <<
		a[0] << ' ' << a[1] << ' ' << a[2] << ' ' << a[3];
}

#endif
#if NATIVE_U16X4 != 0
static stream::out& operator << (
	stream::out& str,
	const u16x4& a) {

	return str <<
		a[0] << ' ' << a[1] << ' ' << a[2] << ' ' << a[3];
}

#endif
// compute the ULP difference between two finite f32's in FTZ mode, up to 24 orders of magnitude
// (larger difference is clamped to 24 orders)
int32_t diff_ulp_ftz(const float a, const float b) {
	const uint32_t ia = reinterpret_cast< const uint32_t& >(a);
	const uint32_t ib = reinterpret_cast< const uint32_t& >(b);
	const uint32_t sign_mask = 0x80000000;
	const uint32_t exp_mask = 0x7f800000;
	const uint32_t mant_mask = ~(sign_mask | exp_mask);

	// early out at identical args or different-sign zeros
	if (ia == ib || sign_mask == (ia | ib))
		return 0;

	// don't bother with a single zero or different-sign non-zero args
	if (0 == (ia & ~sign_mask) || 0 == (ib & ~sign_mask) || (ia & sign_mask) != (ib & sign_mask))
		return -1;

	const uint32_t maxar = max(ia, ib);
	const uint32_t minar = min(ia, ib);
	const uint32_t maxar_exp = maxar >> 23 & 0xff;
	const uint32_t minar_exp = minar >> 23 & 0xff;
	const uint32_t exp_diff = maxar_exp - minar_exp;

	// make sure our same-scale difference fits 64 bits
	if (40 < exp_diff)
		return 1 << 24;

	const uint64_t same_scale_maxar = uint64_t(0x800000 | maxar & mant_mask) << exp_diff;
	const uint64_t same_scale_minar = uint64_t(0x800000 | minar & mant_mask);
	const uint64_t diff = same_scale_maxar - same_scale_minar;

	// clamp differences to 24 orders of magnitude
	return uint32_t(min(diff, uint64_t(1 << 24)));
}

// compute the ULP difference between two finite f32's, up to 24 orders of magnitude
// (larger difference is clamped to 24 orders)
int32_t diff_ulp(const float a, const float b) {
	const uint32_t ia = reinterpret_cast< const uint32_t& >(a);
	const uint32_t ib = reinterpret_cast< const uint32_t& >(b);
	const uint32_t sign_mask = 0x80000000;
	const uint32_t exp_mask = 0x7f800000;
	const uint32_t mant_mask = ~(sign_mask | exp_mask);

	// early out at identical args or different-sign zeros
	if (ia == ib || sign_mask == (ia | ib))
		return 0;

	// don't bother with a single zero or different-sign non-zero args
	if (0 == (ia & ~sign_mask) || 0 == (ib & ~sign_mask) || (ia & sign_mask) != (ib & sign_mask))
		return -1;

	const uint32_t maxar = max(ia, ib);
	const uint32_t minar = min(ia, ib);
	const uint32_t maxar_exp = maxar >> 23 & 0xff;
	const uint32_t minar_exp = minar >> 23 & 0xff;
	const uint32_t maxar_msb = maxar_exp ? 0x800000 : 0;
	const uint32_t minar_msb = minar_exp ? 0x800000 : 0;
	const uint32_t exp_diff = (maxar_exp ? maxar_exp : 1) - (minar_exp ? minar_exp : 1);

	// make sure our same-scale difference fits 64 bits
	if (40 < exp_diff)
		return 1 << 24;

	const uint64_t same_scale_maxar = uint64_t(maxar_msb | maxar & mant_mask) << exp_diff;
	const uint64_t same_scale_minar = uint64_t(minar_msb | minar & mant_mask);
	const uint64_t diff = same_scale_maxar - same_scale_minar;

	// clamp differences to 24 orders of magnitude
	return uint32_t(min(diff, uint64_t(1 << 24)));
}

float compose_f32(const uint32_t sign, const uint32_t exp, const uint32_t mant) {
	const uint32_t ret = (sign << 31) | ((exp & 0xff) << 23) | (mant & 0x7fffff);
	return reinterpret_cast< const float& >(ret);
}

float m0[4][4] __attribute__ ((aligned(64))) = {
	{  0,  1,  2,  3 },
	{  4,  5,  6,  7 },
	{  8,  9, 10, 11 },
	{ 12, 13, 14, 15 }
};
float m1[4][4] __attribute__ ((aligned(64))) = {
	{ 1, 0, 0, 0 },
	{ 0, 1, 0, 0 },
	{ 0, 0, 1, 0 },
	{ 0, 0, 0, 1 }
};

#if NATIVE_F32X8 != 0
f32x8 res[2] __attribute__ ((aligned(64)));

#else
f32x4 res[4] __attribute__ ((aligned(64)));

#endif
int main(int argc, char**) {
	const size_t obfuscate = size_t(argc - 1);

	stream::cin.open(stdin);
	stream::cout.open(stdout);
	stream::cerr.open(stderr);

	stream::cout << std::hex;
	stream::cerr << std::hex;
	bool success = true;

	const size_t reps = 1e9;

	const uint64_t t0 = timer_ns();

#if NATIVE_F32X8 != 0
	for (size_t i = 0; i < reps; ++i) {
		const size_t offset = i * obfuscate;

		// uses permil op under clang and gcc
		const f32x8 m0_01 = reinterpret_cast< const f32x8& >(m0[0 + offset]);
		const f32x8 m0_23 = reinterpret_cast< const f32x8& >(m0[2 + offset]);

		const f32x4 m1_0 = reinterpret_cast< const f32x4& >(m1[0 + offset]);
		const f32x4 m1_1 = reinterpret_cast< const f32x4& >(m1[1 + offset]);
		const f32x4 m1_2 = reinterpret_cast< const f32x4& >(m1[2 + offset]);
		const f32x4 m1_3 = reinterpret_cast< const f32x4& >(m1[3 + offset]);

		// interleave the permils and insert128f's for best scheduling
		const f32x8 m0_00_10 = f32x8(
			m0_01[0], m0_01[0], m0_01[0], m0_01[0],
			m0_01[4], m0_01[4], m0_01[4], m0_01[4]);
		const f32x8 m0_20_30 = f32x8(
			m0_23[0], m0_23[0], m0_23[0], m0_23[0],
			m0_23[4], m0_23[4], m0_23[4], m0_23[4]);

		const f32x8 m1_0x2 = get_f32x8(m1_0, m1_0);

		const f32x8 m0_01_11 = f32x8(
			m0_01[1], m0_01[1], m0_01[1], m0_01[1],
			m0_01[5], m0_01[5], m0_01[5], m0_01[5]);
		const f32x8 m0_21_31 = f32x8(
			m0_23[1], m0_23[1], m0_23[1], m0_23[1],
			m0_23[5], m0_23[5], m0_23[5], m0_23[5]);

		const f32x8 m1_1x2 = get_f32x8(m1_1, m1_1);

		const f32x8 m0_02_12 = f32x8(
			m0_01[2], m0_01[2], m0_01[2], m0_01[2],
			m0_01[6], m0_01[6], m0_01[6], m0_01[6]);
		const f32x8 m0_22_32 = f32x8(
			m0_23[2], m0_23[2], m0_23[2], m0_23[2],
			m0_23[6], m0_23[6], m0_23[6], m0_23[6]);

		const f32x8 m1_2x2 = get_f32x8(m1_2, m1_2);

		const f32x8 m0_03_13 = f32x8(
			m0_01[3], m0_01[3], m0_01[3], m0_01[3],
			m0_01[7], m0_01[7], m0_01[7], m0_01[7]);
		const f32x8 m0_23_33 = f32x8(
			m0_23[3], m0_23[3], m0_23[3], m0_23[3],
			m0_23[7], m0_23[7], m0_23[7], m0_23[7]);

		const f32x8 m1_3x2 = get_f32x8(m1_3, m1_3);

		const f32x8 a01 = m0_00_10 * m1_0x2;
		const f32x8 a23 = m0_20_30 * m1_0x2;
		const f32x8 b01 = m0_01_11 * m1_1x2;
		const f32x8 b23 = m0_21_31 * m1_1x2;
		const f32x8 c01 = m0_02_12 * m1_2x2;
		const f32x8 c23 = m0_22_32 * m1_2x2;
		const f32x8 d01 = m0_03_13 * m1_3x2;
		const f32x8 d23 = m0_23_33 * m1_3x2;

		const f32x8 r01 = a01 + b01 + c01 + d01;
		const f32x8 r23 = a23 + b23 + c23 + d23;

		res[0 + offset] = r01;
		res[1 + offset] = r23;
	}

#else
	for (size_t i = 0; i < reps; ++i) {
		const size_t offset = i * obfuscate;

		const f32x4 m0_0 = reinterpret_cast< const f32x4& >(m0[0 + offset]);
		const f32x4 m0_1 = reinterpret_cast< const f32x4& >(m0[1 + offset]);
		const f32x4 m0_2 = reinterpret_cast< const f32x4& >(m0[2 + offset]);
		const f32x4 m0_3 = reinterpret_cast< const f32x4& >(m0[3 + offset]);

		const f32x4 m1_0 = reinterpret_cast< const f32x4& >(m1[0 + offset]);
		const f32x4 m1_1 = reinterpret_cast< const f32x4& >(m1[1 + offset]);
		const f32x4 m1_2 = reinterpret_cast< const f32x4& >(m1[2 + offset]);
		const f32x4 m1_3 = reinterpret_cast< const f32x4& >(m1[3 + offset]);

		const f32x4 a0 = f32x4(m0_0[0]) * m1_0;
		const f32x4 a1 = f32x4(m0_1[0]) * m1_0;
		const f32x4 a2 = f32x4(m0_2[0]) * m1_0;
		const f32x4 a3 = f32x4(m0_3[0]) * m1_0;

		const f32x4 b0 = f32x4(m0_0[1]) * m1_1;
		const f32x4 b1 = f32x4(m0_1[1]) * m1_1;
		const f32x4 b2 = f32x4(m0_2[1]) * m1_1;
		const f32x4 b3 = f32x4(m0_3[1]) * m1_1;

		const f32x4 c0 = f32x4(m0_0[2]) * m1_2;
		const f32x4 c1 = f32x4(m0_1[2]) * m1_2;
		const f32x4 c2 = f32x4(m0_2[2]) * m1_2;
		const f32x4 c3 = f32x4(m0_3[2]) * m1_2;

		const f32x4 d0 = f32x4(m0_0[3]) * m1_3;
		const f32x4 d1 = f32x4(m0_1[3]) * m1_3;
		const f32x4 d2 = f32x4(m0_2[3]) * m1_3;
		const f32x4 d3 = f32x4(m0_3[3]) * m1_3;

		const f32x4 r0 = a0 + b0 + c0 + d0;
		const f32x4 r1 = a1 + b1 + c1 + d1;
		const f32x4 r2 = a2 + b2 + c2 + d2;
		const f32x4 r3 = a3 + b3 + c3 + d3;

		res[0 + offset] = r0;
		res[1 + offset] = r1;
		res[2 + offset] = r2;
		res[3 + offset] = r3;
	}

#endif
	const uint64_t dt = timer_ns() - t0;

#if NATIVE_F32X8 != 0
	stream::cout <<
		res[0] << '\n' <<
		res[1] << '\n';

#else
	stream::cout <<
		res[0] << '\n' <<
		res[1] << '\n' <<
		res[2] << '\n' <<
		res[3] << '\n';

#endif
	stream::cout << "elapsed: " << dt / 1e9 << '\n';
	return success ? 0 : -1;
}
