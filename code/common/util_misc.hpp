#ifndef util_misc_H__
#define util_misc_H__

#include <stdio.h>
#if PLATFORM_GL
	#include "native_gl.h"
#else
	#include <GLES2/gl2.h>
#endif

#include <string>

#define DEBUG_GL_ERR()                                           \
	if (DEBUG_LITERAL && util::reportGLError())                  \
	{                                                            \
		std::cerr << __FILE__ ":" XQUOTE(__LINE__) << std::endl; \
		return false;                                            \
	}

namespace util {

template < bool >
struct compile_assert;

template <>
struct compile_assert< true > {
	compile_assert() {
	}
};

bool setupShader(
	const GLuint shader_name,
	const char* const filename);

bool setupShaderWithPatch(
	const GLuint shader_name,
	const char* const filename,
	const size_t patch_count,
	const std::string* const patch);

bool setupProgram(
	const GLuint prog,
	const GLuint shader_vert,
	const GLuint shader_frag);

bool reportGLError(FILE* file = stderr);
bool reportEGLError(FILE* file = stderr);

} // namespace util

#endif // util_misc_H__
