#if _WIN32
#include <GL/glew.h>
#endif
#include <GL/freeglut.h>
#include "param.h"
#include "stream.hpp"

namespace { // anonymous

void reshape(GLsizei w, GLsizei h)
{
	glViewport(0, 0, w, h);
}

void display()
{
	static bool once = true;
	if (once) {
		once = false;
		if (0 == initFrameLoop()) {
			glutDestroyWindow(glutGetWindow());
			return;
		}
	}

	if (0 == param.frames-- || 0 == renderFrame()) {
		glutDestroyWindow(glutGetWindow());
		return;
	}

	glutSwapBuffers();
	glutPostRedisplay();
}

void kbd(unsigned char key, int x, int y)
{
#if _WIN32 == 0 // on Windows this handler is not called from the main thread message loop -- don't attempt to modify the context
	static int isFullscreen;
	static char fullscreen[64];

	switch (key) {
	case 'f':
	case 'F':
		if ('\0' == fullscreen[0]) {
			sprintf(fullscreen, "%dx%d", param.image_w, param.image_h);
			glutGameModeString(fullscreen);
		}

		if (0 == deinitFrameLoop()) {
			glutDestroyWindow(glutGetWindow());
			return;
		}

		if (isFullscreen)
			glutLeaveGameMode();
		else
			glutEnterGameMode();

		if (0 == initFrameLoop()) {
			glutDestroyWindow(glutGetWindow());
			return;
		}

		isFullscreen = ~isFullscreen;
		break;
	}

#endif
}

void handler_exit()
{
#if _WIN32 == 0 // on Windows this handler is not called from the main thread message loop -- don't attempt to modify the context
	deinitFrameLoop();

#endif
}

} // namespace

int main(int argc, char **argv)
{
	// set up cin, cout and cerr substitute streams
	stream::cin.open(stdin);
	stream::cout.open(stdout);
	stream::cerr.open(stderr);

#if FB_RES_FIXED_W
	param.image_w = FB_RES_FIXED_W;

#else
	param.image_w = 512;

#endif
#if FB_RES_FIXED_H
	param.image_h = FB_RES_FIXED_H;

#else
	param.image_h = 512;

#endif
	param.bitness[0] = 8;
	param.bitness[1] = 8;
	param.bitness[2] = 8;
	param.bitness[3] = 0;
	param.fsaa = 0;
	param.frames = -1U;

	// read render setup from CLI
	const int result_cli = parseCLI(argc, argv, &param);

	if (0 != result_cli)
		return result_cli;

	glutInit(&argc, argv);
	unsigned glutModeFlags = GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH;
	if (param.fsaa) {
		glutSetOption(GLUT_MULTISAMPLE, param.fsaa);
		glutModeFlags |= GLUT_MULTISAMPLE;
	}

	glutInitDisplayMode(glutModeFlags);
	glutInitContextVersion(3, 2);
	glutInitContextFlags(GLUT_FORWARD_COMPATIBLE);
	glutInitContextProfile(GLUT_CORE_PROFILE);

	glutInitWindowPosition(0, 0);
	glutInitWindowSize(param.image_w, param.image_h);
	glutCreateWindow(argv[0]);

	glutReshapeFunc(reshape);
	glutDisplayFunc(display);
	glutKeyboardFunc(kbd);

#if _WIN32
	GLenum err = glewInit();
	if (GLEW_OK != err) {
		fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
		return -1;
	}

#endif
	if (atexit(handler_exit)) {
		printf("error: could not register an exit routine with atexit()\n");
		return -1;
	}

	glutMainLoop();
	return 0;
}
