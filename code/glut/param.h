#ifndef param_H__
#define param_H__

struct Param {
	unsigned image_w;       // frame width
	unsigned image_h;       // frame height
	unsigned bitness[4];    // rgba bitness
	unsigned fsaa;          // fsaa number of samples
	unsigned frames;        // frames to run
};

#ifdef __cplusplus
#define C_MANGLE "C"
#else
#define C_MANGLE
#endif

extern C_MANGLE struct Param param;

extern C_MANGLE int parseCLI(const int, char** const, struct Param*);
extern C_MANGLE int initFrameLoop(void);
extern C_MANGLE int renderFrame(void);
extern C_MANGLE int deinitFrameLoop(void);

#undef C_MANGLE

#endif // param_H__
