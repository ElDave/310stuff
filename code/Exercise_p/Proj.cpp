#if PLATFORM_GL
	#include "native_gl.h"
	#include "gles_gl_mapping.hpp"
#else
	#include <EGL/egl.h>
	#include <GLES2/gl2.h>
	#include <GLES2/gl2ext.h>
	#include "gles_ext.hpp"
#endif

#include <unistd.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#if _WIN32
#define _USE_MATH_DEFINES
#endif
#include <math.h>
#include <string>
#include <iostream>

#include "param.h"
#include "scoped.hpp"
#include "util_tex.hpp"
#include "util_misc.hpp"
#include "pure_macro.hpp"

#include "rendSkeleton.hpp"
#include "rendVertAttr.hpp"

using testbed::scoped_ptr;
using testbed::scoped_functor;
using testbed::deinit_resources_t;

namespace {

#define SETUP_VERTEX_ATTR_POINTERS_MASK	( \
		SETUP_VERTEX_ATTR_POINTERS_MASK_vertex | \
		SETUP_VERTEX_ATTR_POINTERS_MASK_normal | \
		SETUP_VERTEX_ATTR_POINTERS_MASK_blendw) /* | \
		SETUP_VERTEX_ATTR_POINTERS_MASK_tcoord) */

#include "rendVertAttr_setupVertAttrPointers.hpp"
#undef SETUP_VERTEX_ATTR_POINTERS_MASK

struct Vertex {
	GLfloat pos[3];
	GLfloat nrm[3];
	GLfloat	bon[4];
#if 0
	GLfloat txc[2];
#endif
};

const char arg_prefix[]    = "-";
const char arg_app[]       = "app";

const char arg_normal[]    = "normal_map";
const char arg_albedo[]    = "albedo_map";
const char arg_alt_anim[]  = "alt_anim";
const char arg_anim_step[] = "anim_step";

struct TexDesc {
	const char* filename;
	unsigned w;
	unsigned h;
};

TexDesc g_normal = { "ice.raw", 500, 360 };
TexDesc g_albedo = { "ice.raw", 500, 360 };

const unsigned bone_count = 4;

rend::Bone g_bone[bone_count];
rend::dense_matx4 g_bone_mat[bone_count];
std::vector< rend::Track > g_skeletal_animation;
float g_anim_step = .125f * .125f * .125f;
bool g_alt_anim;

#if PLATFORM_EGL
EGLDisplay g_display = EGL_NO_DISPLAY;
EGLContext g_context = EGL_NO_CONTEXT;

#endif

enum {
	TEX_NORMAL,
	TEX_ALBEDO,

	TEX_COUNT,
	TEX_FORCE_UINT = -1U
};

enum {
	PROG_SKIN,

	PROG_COUNT,
	PROG_FORCE_UINT = -1U
};

enum {
	UNI_SAMPLER_NORMAL,
	UNI_SAMPLER_ALBEDO,

	UNI_LP_OBJ,
	UNI_VP_OBJ,
	UNI_BONE,
	UNI_MVP,

	UNI_COUNT,
	UNI_FORCE_UINT = -1U
};

enum {
	MESH_SKIN,

	MESH_COUNT,
	MESH_FORCE_UINT = -1U
};

enum {
	VBO_SKIN_VTX,
	VBO_SKIN_IDX,

	VBO_COUNT,
	VBO_FORCE_UINT = -1U
};

GLint g_uni[PROG_COUNT][UNI_COUNT];

#if PLATFORM_GL_OES_vertex_array_object
GLuint g_vao[PROG_COUNT];

#endif
GLuint g_tex[TEX_COUNT];
GLuint g_vbo[VBO_COUNT];
GLuint g_shader_vert[PROG_COUNT];
GLuint g_shader_frag[PROG_COUNT];
GLuint g_shader_prog[PROG_COUNT];

unsigned g_num_faces[MESH_COUNT];

rend::ActiveAttrSemantics g_active_attr_semantics[PROG_COUNT];

} // namespace

int parse_cli(
	const unsigned argc,
	const char* const* argv)
{
	bool cli_err = false;
	const unsigned prefix_len = strlen(arg_prefix);

	for (unsigned i = 1; i < argc && !cli_err; ++i) {
		if (strncmp(argv[i], arg_prefix, prefix_len) ||
			strcmp(argv[i] + prefix_len, arg_app)) {
			continue;
		}

		if (++i < argc) {
			if (i + 3 < argc && !strcmp(argv[i], arg_normal)) {
				if (1 == sscanf(argv[i + 2], "%u", &g_normal.w) &&
					1 == sscanf(argv[i + 3], "%u", &g_normal.h)) {

					g_normal.filename = argv[i + 1];
					i += 3;
					continue;
				}
			}
			else
			if (i + 3 < argc && !strcmp(argv[i], arg_albedo)) {
				if (1 == sscanf(argv[i + 2], "%u", &g_albedo.w) &&
					1 == sscanf(argv[i + 3], "%u", &g_albedo.h)) {

					g_albedo.filename = argv[i + 1];
					i += 3;
					continue;
				}
			}
			else
			if (i + 1 < argc && !strcmp(argv[i], arg_anim_step)) {
				if (1 == sscanf(argv[i + 1], "%f", &g_anim_step) && 0.f < g_anim_step) {
					i += 1;
					continue;
				}
			}
			else
			if (!strcmp(argv[i], arg_alt_anim)) {
				g_alt_anim = true;
				continue;
			}
		}

		cli_err = true;
	}

	if (cli_err) {
		std::cerr << "app options:\n"
			"\t" << arg_prefix << arg_app << " " << arg_normal <<
			" <filename> <width> <height>\t: use specified raw file and dimensions as source of normal map\n"
			"\t" << arg_prefix << arg_app << " " << arg_albedo <<
			" <filename> <width> <height>\t: use specified raw file and dimensions as source of albedo map\n"
			"\t" << arg_prefix << arg_app << " " << arg_alt_anim <<
			"\t\t\t\t\t: use alternative skeleton animation\n"
			"\t" << arg_prefix << arg_app << " " << arg_anim_step <<
			" <step>\t\t\t\t: use specified animation step; entire animation is 1.0\n" << std::endl;
	}

	return cli_err;
}

namespace { // anonymous

// set up skeleton animation, type A
void setupSkeletonAnimA()
{
	
}

// set up skeleton animation, type B
void setupSkeletonAnimB()
{
	const simd::quat identq(0.f, 0.f, 0.f, 1.f);
	
	static const uint8_t anim_bone[] =
	{
		1,2,
	};

	for (unsigned i = 0; i < sizeof(anim_bone) / sizeof(anim_bone[0]); ++i)
	{
		const simd::vect3& axis = simd::vect3(0.f, 0.f, 1.f);

		float angle;
		if (i == 0) angle = -0.5f;
		else angle = 0.5;
		rend::Track& track = *g_skeletal_animation.insert(g_skeletal_animation.end(), rend::Track());

		
		track.bone_idx = anim_bone[i];

		rend::BoneOrientationKey& ori0 =
			*track.orientation_key.insert(track.orientation_key.end(), rend::BoneOrientationKey());

		ori0.time = 0.f;
		ori0.value = identq;

		rend::BoneOrientationKey& ori1 =
			*track.orientation_key.insert(track.orientation_key.end(), rend::BoneOrientationKey());

		ori1.time = .25f;
		ori1.value = simd::quat(M_PI * angle, simd::vect3(0.f, 0.f, 1.f));
		rend::BoneOrientationKey& ori3 =
			*track.orientation_key.insert(track.orientation_key.end(), rend::BoneOrientationKey());
		ori3.time = .5f;
		ori3.value = simd::quat(M_PI * angle*1.125f, simd::vect3(0.f, 1.f, 0.f));
	


	


		

		rend::BoneOrientationKey& ori5 =
			*track.orientation_key.insert(track.orientation_key.end(), rend::BoneOrientationKey());
		ori5.time = 1.f;
		ori5.value = ori3.value;

		track.position_last_key_idx = 0;
		track.orientation_last_key_idx = 0;
		track.scale_last_key_idx = 0;
	}
	static const uint8_t anim_bone2[] =
	{
		3,4
	};
	for (unsigned i = 0; i < sizeof(anim_bone2) / sizeof(anim_bone2[0]); ++i)
	{
		rend::Track& track = *g_skeletal_animation.insert(g_skeletal_animation.end(), rend::Track());
		track.bone_idx = anim_bone2[i];
	    rend::BoneScaleKey& scale0 = *track.scale_key.insert(track.scale_key.end(), rend::BoneScaleKey());
		scale0.time = 0.0f;
		scale0.value = simd::vect3(1.f, 1.f, 1.f);
		

	rend::BoneOrientationKey& ori1 =
	*track.orientation_key.insert(track.orientation_key.end(), rend::BoneOrientationKey());
	ori1.time = 0.5f;
	
	rend::BoneScaleKey& scale1 = *track.scale_key.insert(track.scale_key.end(), rend::BoneScaleKey());
	scale1.time = 0.90f;
	scale1.value = simd::vect3(4.f, 4.f, 4.f);
	track.position_last_key_idx = 0;
	track.orientation_last_key_idx = 0;
	track.scale_last_key_idx = 0;
	}
}

bool check_context(
	const char* prefix)
{
	bool context_correct = true;

#if PLATFORM_EGL
	if (g_display != eglGetCurrentDisplay()) {
		std::cerr << prefix << " encountered foreign display" << std::endl;
		context_correct = false;
	}

	if (g_context != eglGetCurrentContext()) {
		std::cerr << prefix << " encountered foreign context" << std::endl;
		context_correct = false;
	}

#endif
	return context_correct;
}

} // namespace

int deinitFrameLoop()
{
	if (!check_context(__FUNCTION__))
		return false;

	for (unsigned i = 0; i < sizeof(g_shader_prog) / sizeof(g_shader_prog[0]); ++i)
	{
		glDeleteProgram(g_shader_prog[i]);
		g_shader_prog[i] = 0;
	}

	for (unsigned i = 0; i < sizeof(g_shader_vert) / sizeof(g_shader_vert[0]); ++i)
	{
		glDeleteShader(g_shader_vert[i]);
		g_shader_vert[i] = 0;
	}

	for (unsigned i = 0; i < sizeof(g_shader_frag) / sizeof(g_shader_frag[0]); ++i)
	{
		glDeleteShader(g_shader_frag[i]);
		g_shader_frag[i] = 0;
	}

	glDeleteTextures(sizeof(g_tex) / sizeof(g_tex[0]), g_tex);
	memset(g_tex, 0, sizeof(g_tex));

#if PLATFORM_GL_OES_vertex_array_object
	glDeleteVertexArraysOES(sizeof(g_vao) / sizeof(g_vao[0]), g_vao);
	memset(g_vao, 0, sizeof(g_vao));

#endif
	glDeleteBuffers(sizeof(g_vbo) / sizeof(g_vbo[0]), g_vbo);
	memset(g_vbo, 0, sizeof(g_vbo));

#if PLATFORM_EGL
	g_display = EGL_NO_DISPLAY;
	g_context = EGL_NO_CONTEXT;

#endif
	return true;
}

namespace { // anonymous

#if DEBUG && PLATFORM_GL_KHR_debug
void debugProc(
	GLenum source,
	GLenum type,
	GLuint id,
	GLenum severity,
	GLsizei length,
	const GLchar* message,
	const void* userParam)
{
	fprintf(stderr, "log: %s\n", message);
}

#endif
bool deinit_resources() {
	return deinitFrameLoop();
}

} // namespace

int initFrameLoop()
{
#if DEBUG && PLATFORM_GL_KHR_debug
	glDebugMessageCallbackKHR(debugProc, NULL);
	glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS_KHR);
	glEnable(GL_DEBUG_OUTPUT_KHR);
	DEBUG_GL_ERR()

	glDebugMessageInsertKHR(
		GL_DEBUG_SOURCE_APPLICATION_KHR,
		GL_DEBUG_TYPE_OTHER_KHR,
		GLuint(42),
		GL_DEBUG_SEVERITY_HIGH_KHR,
		GLint(-1), 
		"testing 1, 2, 3");
	DEBUG_GL_ERR()

#endif
#if PLATFORM_EGL
	g_display = eglGetCurrentDisplay();

	if (EGL_NO_DISPLAY == g_display) {
		std::cerr << __FUNCTION__ << " encountered nil display" << std::endl;
		return false;
	}

	g_context = eglGetCurrentContext();

	if (EGL_NO_CONTEXT == g_context) {
		std::cerr << __FUNCTION__ << " encountered nil context" << std::endl;
		return false;
	}

#endif
	scoped_ptr< deinit_resources_t, scoped_functor > on_error(deinit_resources);

	/////////////////////////////////////////////////////////////////
	// set up misc control bits and values

	glDisable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	
	const GLclampf red = .25f;
	const GLclampf green = .25f;
	const GLclampf blue = .25f;
	const GLclampf alpha = 1.f;

	glClearColor(red, green, blue, alpha);
	glClearDepthf(1.f);

	/////////////////////////////////////////////////////////////////
	// reserve all necessary texture objects

	glGenTextures(sizeof(g_tex) / sizeof(g_tex[0]), g_tex);

	for (unsigned i = 0; i < sizeof(g_tex) / sizeof(g_tex[0]); ++i)
		assert(g_tex[i]);

	/////////////////////////////////////////////////////////////////
	// load textures

	if (!util::setupTexture2D(g_tex[TEX_NORMAL], g_normal.filename, g_normal.w, g_normal.h)) {
		std::cerr << __FUNCTION__ << " failed at setupTexture2D" << std::endl;
		return false;
	}

	if (!util::setupTexture2D(g_tex[TEX_ALBEDO], g_albedo.filename, g_albedo.w, g_albedo.h)) {
		std::cerr << __FUNCTION__ << " failed at setupTexture2D" << std::endl;
		return false;
	}

	/////////////////////////////////////////////////////////////////
	// init the program/uniforms matrix to all empty

	for (unsigned i = 0; i < PROG_COUNT; ++i)
		for (unsigned j = 0; j < UNI_COUNT; ++j)
			g_uni[i][j] = -1;

	/////////////////////////////////////////////////////////////////
	// create the shader program from two shaders

	g_shader_vert[PROG_SKIN] = glCreateShader(GL_VERTEX_SHADER);
	assert(g_shader_vert[PROG_SKIN]);

	if (!util::setupShader(g_shader_vert[PROG_SKIN], "phong_skinning_bump_tang.glslv")) {
		std::cerr << __FUNCTION__ << " failed at setupShader" << std::endl;
		return false;
	}

	g_shader_frag[PROG_SKIN] = glCreateShader(GL_FRAGMENT_SHADER);
	assert(g_shader_frag[PROG_SKIN]);

	if (!util::setupShader(g_shader_frag[PROG_SKIN], "phong_bump_tang.glslf")) {
		std::cerr << __FUNCTION__ << " failed at setupShader" << std::endl;
		return false;
	}

	g_shader_prog[PROG_SKIN] = glCreateProgram();
	assert(g_shader_prog[PROG_SKIN]);

	if (!util::setupProgram(
			g_shader_prog[PROG_SKIN],
			g_shader_vert[PROG_SKIN],
			g_shader_frag[PROG_SKIN]))
	{
		std::cerr << __FUNCTION__ << " failed at setupProgram" << std::endl;
		return false;
	}

	/////////////////////////////////////////////////////////////////
	// query the program about known uniform vars and vertex attribs

	g_uni[PROG_SKIN][UNI_MVP]    = glGetUniformLocation(g_shader_prog[PROG_SKIN], "mvp");
	g_uni[PROG_SKIN][UNI_BONE]   = glGetUniformLocation(g_shader_prog[PROG_SKIN], "bone");
	g_uni[PROG_SKIN][UNI_LP_OBJ] = glGetUniformLocation(g_shader_prog[PROG_SKIN], "lp_obj");
	g_uni[PROG_SKIN][UNI_VP_OBJ] = glGetUniformLocation(g_shader_prog[PROG_SKIN], "vp_obj");

	g_uni[PROG_SKIN][UNI_SAMPLER_NORMAL] = glGetUniformLocation(g_shader_prog[PROG_SKIN], "normal_map");
	g_uni[PROG_SKIN][UNI_SAMPLER_ALBEDO] = glGetUniformLocation(g_shader_prog[PROG_SKIN], "albedo_map");

	g_active_attr_semantics[PROG_SKIN].registerVertexAttr(glGetAttribLocation(g_shader_prog[PROG_SKIN], "at_Vertex"));
	g_active_attr_semantics[PROG_SKIN].registerNormalAttr(glGetAttribLocation(g_shader_prog[PROG_SKIN], "at_Normal"));
	g_active_attr_semantics[PROG_SKIN].registerBlendWAttr(glGetAttribLocation(g_shader_prog[PROG_SKIN], "at_Weight"));
	g_active_attr_semantics[PROG_SKIN].registerTCoordAttr(glGetAttribLocation(g_shader_prog[PROG_SKIN], "at_MultiTexCoord0"));


	const float bolen = .25f;

	const simd::quat identq(0.f, 0.f, 0.f, 1.f);
	const simd::vect3 idents(1.f, 1.f, 1.f);

	g_bone[0].position = simd::vect3(0.f, 0.f, 0.f);
	g_bone[0].orientation = identq;
	g_bone[0].scale = idents;
	g_bone[0].parent_idx = 255;

	g_bone[1].position = simd::vect3(0.63f, 0.125f, 0.f);
	g_bone[1].orientation = identq;
	g_bone[1].scale = idents;
	g_bone[1].parent_idx = 0;

	g_bone[2].position = simd::vect3(-0.63f, 0.125f, 0.f);
	g_bone[2].orientation = identq;
	g_bone[2].scale = idents;
	g_bone[2].parent_idx = 0;

	g_bone[3].position = simd::vect3(0.9f, -0.25f, 0.f);
	g_bone[3].orientation = identq;
	g_bone[3].scale = idents;
	g_bone[3].parent_idx = 1;

	g_bone[4].position = simd::vect3(-0.9f, -0.25f, 0.f);
	g_bone[4].orientation = identq;
	g_bone[4].scale = idents;
	g_bone[4].parent_idx = 2;

	for (unsigned i = 0; i < bone_count; ++i)
		rend::initBoneMatx(bone_count, g_bone_mat, g_bone, i);

	if (g_alt_anim)
		setupSkeletonAnimB();
	else
		setupSkeletonAnimB();

	/////////////////////////////////////////////////////////////////
	// set up mesh of { pos, nrm, bon, txc }
	//define many many constants
	const float x = 0.433013f;
	const float hx = 0.6f;
	const float z = 0.25f;
	const float y = 0.65f;
	const float yy = 0.4f;
	const float hy = 0.125f;
	const float ybase = 0.68f;
	const float ybotbase = 0.80f;
	const float zz = 0.5f;
	const uint16_t o = 6;//offset
	const float s = 1.75f;//bottom scale
	const float ss = 1.25f;//decline scale
	const float sms = 1.125f;
	const float legscale = 0.75f;
	const float handscale = 0.5f;
	const float slegscale = 0.3f;
	const float legoffset = 0.4f;
	static const Vertex arri[] =
	{
		{ { 0,0.9,0 },{ 0.f, 0.f, 1.f },{ 0.f, 0.f, 0.f, 0.f } },//tip
	{ { x,y,z },{ 0.f, 0.f, 1.f },{ 0.f, 0.f, 0.f, 0.f } },//top hexa
	{ { -x,y,z },{ 0.f, 0.f, 1.f },{ 0.f, 0.f, 0.f, 0.f } },
	{ { x,y,-z },{ 0.f, 0.f, 1.f },{ 0.f, 0.f, 0.f, 0.f } },
	{ { -x,y,-z },{ 0.f, 0.f, 1.f },{ 0.f, 0.f, 0.f, 0.f } },
	{ { 0,y,zz },{ 0.f, 0.f, 1.f },{ 0.f, 0.f, 0.f, 0.f } },
	{ { 0,y,-zz },{ 0.f, 0.f, 1.f },{ 0.f, 0.f, 0.f, 0.f } },//top hexa
	{ { s*x,-yy,s*z },{ 0.f, 0.f, 1.f },{ 0.f, 0.f, 0.f, 0.f } },//bot hexa
	{ { -x * s,-yy,z*s },{ 0.f, 0.f, 1.f },{ 0.f, 0.f, 0.f, 0.f } },
	{ { x*s,-yy,-z * s },{ 0.f, 0.f, 1.f },{ 0.f, 0.f, 0.f, 0.f } },
	{ { -x * s,-yy,-z * s },{ 0.f, 0.f, 1.f },{ 0.f, 0.f, 0.f, 0.f } },
	{ { 0,-yy,zz*s },{ 0.f, 0.f, 1.f },{ 0.f, 0.f, 0.f, 0.f } },
	{ { 0,-yy,-zz * s },{ 0.f, 0.f, 1.f },{ 0.f, 0.f, 0.f, 0.f } },//bot hexa
	{ { ss*x,-y,ss*z },{ 0.f, 0.f, 1.f },{ 0.f, 0.f, 0.f, 0.f } }, //botton decline
	{ { -x * ss,-y,z*ss },{ 0.f, 0.f, 1.f },{ 0.f, 0.f, 0.f, 0.f } },
	{ { x*ss,-y,-z * ss },{ 0.f, 0.f, 1.f },{ 0.f, 0.f, 0.f, 0.f } },
	{ { -x * ss,-y,-z * ss },{ 0.f, 0.f, 1.f },{ 0.f, 0.f, 0.f, 0.f } },
	{ { 0,-y,zz*ss },{ 0.f, 0.f, 1.f },{ 0.f, 0.f, 0.f, 0.f } },
	{ { 0,-y,-zz * ss },{ 0.f, 0.f, 1.f },{ 0.f, 0.f, 0.f, 0.f } },
	{ { ((x*legscale) + legoffset),-ybase,(z*legscale) },{ -1.f, -1.f, -1.f },{ 0.f, 0.f, 0.f, 0.f } },//rightlegtopbase
	{ { ((-x * legscale) + legoffset),-ybase,(z*legscale) },{ -1.f, -1.f, -1.f },{ 0.f, 0.f, 0.f, 0.f } },
	{ { ((x*legscale) + legoffset),-ybase,(-z * legscale) },{ -1.f, -1.f, -1.f },{ 0.f, 0.f, 0.f, 0.f } },
	{ { ((-x * legscale) + legoffset) ,-ybase,(-z * legscale) },{ -1.f, -1.f, -1.f },{ 0.f, 0.f, 0.f, 0.f } },
	{ { legoffset,-ybase,((zz*legscale)) },{ -1.f, -1.f, -1.f },{ 0.f, 0.f, 0.f, 0.f } },
	{ { legoffset,-ybase,((-zz * legscale)) },{ -1.f, -1.f, -1.f },{ 0.f, 0.f, 0.f, 0.f } },//rightlegtopbase
	{ { ((x*legscale) + legoffset),-ybotbase,(z*legscale) },{ -1.f, -1.f, -1.f },{ 0.f, 0.f, 0.f, 0.f } },//rightlegbotbase
	{ { ((-x * legscale) + legoffset),-ybotbase,(z*legscale) },{ -1.f, -1.f, -1.f },{ 0.f, 0.f, 0.f, 0.f } },
	{ { ((x*legscale) + legoffset),-ybotbase,(-z * legscale) },{ -1.f, -1.f, -1.f },{ 0.f, 0.f, 0.f, 0.f } },
	{ { ((-x * legscale) + legoffset) ,-ybotbase,(-z * legscale) },{ -1.f, -1.f, -1.f },{ 0.f, 0.f, 0.f, 0.f } },
	{ { legoffset,-ybotbase,((zz*legscale)) },{ -1.f, -1.f, -1.f },{ 0.f, 0.f, 0.f, 0.f } },
	{ { legoffset,-ybotbase,((-zz * legscale)) },{ -1.f, -1.f, -1.f },{ 0.f, 0.f, 0.f, 0.f } },//rightlegbotbase
	{ { ((x*legscale) - legoffset),-ybase,(z*legscale) },{ -1.f, -1.f, -1.f },{ 0.f, 0.f, 0.f, 0.f } },//leftlegtopbase
	{ { ((-x * legscale) - legoffset),-ybase,(z*legscale) },{ -1.f, -1.f, -1.f },{ 0.f, 0.f, 0.f, 0.f } },
	{ { ((x*legscale) - legoffset),-ybase,(-z * legscale) },{ -1.f, -1.f, -1.f },{ 0.f, 0.f, 0.f, 0.f } },
	{ { ((-x * legscale) - legoffset) ,-ybase,(-z * legscale) },{ -1.f, -1.f, -1.f },{ 0.f, 0.f, 0.f, 0.f } },
	{ { -legoffset,-ybase,((zz*legscale)) },{ -1.f, -1.f, -1.f },{ 0.f, 0.f, 0.f, 0.f } },
	{ { -legoffset,-ybase,((-zz * legscale)) },{ -1.f, -1.f, -1.f },{ 0.f, 0.f, 0.f, 0.f } },//leftlegtopbase
	{ { ((x*legscale) - legoffset),-ybotbase,(z*legscale) },{ -1.f, -1.f, -1.f },{ 0.f, 0.f, 0.f, 0.f } },//leftlegbotbase
	{ { ((-x * legscale) - legoffset),-ybotbase,(z*legscale) },{ -1.f, -1.f, -1.f },{ 0.f, 0.f, 0.f, 0.f } },
	{ { ((x*legscale) - legoffset),-ybotbase,(-z * legscale) },{ -1.f, -1.f, -1.f },{ 0.f, 0.f, 0.f, 0.f } },
	{ { ((-x * legscale) - legoffset) ,-ybotbase,(-z * legscale) },{ -1.f, -1.f, -1.f },{ 0.f, 0.f, 0.f, 0.f } },
	{ { -legoffset,-ybotbase,((zz*legscale)) },{ -1.f, -1.f, -1.f },{ 0.f, 0.f, 0.f, 0.f } },
	{ { -legoffset,-ybotbase,((-zz * legscale)) },{ -1.f, -1.f, -1.f },{ 0.f, 0.f, 0.f, 0.f } },//leftlegbotbase
	{ { legoffset,-1.4f, },{ -1.f, -1.f, -1.f },{ 0.f, 0.f, 0.f, 0.f } },//rightlegbotbase
	{ { ((x*slegscale) + legoffset),-ybotbase,(z*slegscale) },{ -1.f, -1.f, -1.f },{ 0.f, 0.f, 0.f, 0.f } },
	{ { ((-x * slegscale) + legoffset),-ybotbase,(z*slegscale) },{ -1.f, -1.f, -1.f },{ 0.f, 0.f, 0.f, 0.f } },
	{ { ((x*slegscale) + legoffset),-ybotbase,(-z * slegscale) },{ -1.f, -1.f, -1.f },{ 0.f, 0.f, 0.f, 0.f } },
	{ { ((-x * slegscale) + legoffset) ,-ybotbase,(-z * slegscale) },{ -1.f, -1.f, -1.f },{ 0.f, 0.f, 0.f, 0.f } },
	{ { legoffset,-ybotbase,((zz*slegscale)) },{ -1.f, -1.f, -1.f },{ 0.f, 0.f, 0.f, 0.f } },
	{ { legoffset,-ybotbase,((-zz * slegscale)) },{ -1.f, -1.f, -1.f },{ 0.f, 0.f, 0.f, 0.f } },//rightlegbotbase
	{ { -legoffset,-1.4f, },{ -1.f, -1.f, -1.f },{ 0.f, 0.f, 0.f, 0.f } },//leftlegbotbase
	{ { ((x*slegscale) - legoffset),-ybotbase,(z*slegscale) },{ -1.f, -1.f, -1.f },{ 0.f, 0.f, 0.f, 0.f } },
	{ { ((-x * slegscale) - legoffset),-ybotbase,(z*slegscale) },{ -1.f, -1.f, -1.f },{ 0.f, 0.f, 0.f, 0.f } },
	{ { ((x*slegscale) - legoffset),-ybotbase,(-z * slegscale) },{ -1.f, -1.f, -1.f },{ 0.f, 0.f, 0.f, 0.f } },
	{ { ((-x * slegscale) - legoffset) ,-ybotbase,(-z * slegscale) },{ -1.f, -1.f, -1.f },{ 0.f, 0.f, 0.f, 0.f } },
	{ { -legoffset,-ybotbase,((zz*slegscale)) },{ -1.f, -1.f, -1.f },{ 0.f, 0.f, 0.f, 0.f } },
	{ { -legoffset,-ybotbase,((-zz * slegscale)) },{ -1.f, -1.f, -1.f },{ 0.f, 0.f, 0.f, 0.f } },//leftlegbotbase
	{ { hx,hy,0 },{ 0.f, 0.f, 1.f },{ 1.f, 0.f, 0.f, 0.f } },//rightarmbase
	{ { hx*(sms)+(2 * hy),hy + (handscale*z),x*handscale },{ 0.f, 0.f, 1.f },{ 1.f, 0.f, 0.f, 1.f } } ,
	{ { hx*(sms)+(2 * hy),hy + (handscale*z),-x * handscale },{ 0.f, 0.f, 1.f },{ 1.f, 0.f, 0.f, 1.f } },
	{ { hx*(sms)+(2 * hy),hy - (handscale*z),x*handscale },{ 0.f, 0.f, 1.f },{ 1.f, 0.f, 0.f, 1.f } },
	{ { hx*(sms)+(2 * hy),hy - (handscale*z),-x * handscale },{ 0.f, 0.f, 1.f },{ 1.f, 0.f, 0.f, 1.f } },
	{ { hx*(sms)+(2 * hy),hy + (zz*handscale),0 },{ 0.f, 0.f, 1.f },{ 1.f, 0.f, 0.f, 1.f } },
	{ { hx*(sms)+(2 * hy),hy + (-zz * handscale),0 },{ 0.f, 0.f, 1.f },{ 1.f, 0.f, 0.f, 1.f } },//rightarmbase
	{ { 1.5f,hy + (handscale*z),x*handscale },{ 0.f, 0.f, 1.f },{ 0.8f, 0.1f, 0.1f, 3.f } } ,//rightarmshaft
	{ { 1.5f,hy + (handscale*z),-x * handscale },{ 0.f, 0.f, 1.f },{ 0.8f, 0.1f, 0.1f, 3.f } },
	{ { 1.5f,hy - (handscale*z),x*handscale },{ 0.f, 0.f, 1.f },{ 0.8f, 0.1f, 0.1f, 3.f } },
	{ { 1.5f,hy - (handscale*z),-x * handscale },{ 0.f, 0.f, 1.f },{ 0.8f, 0.1f, 0.1f, 3.f } },
	{ { 1.5f,hy + (zz*handscale),0 },{ 0.f, 0.f, 1.f },{ 0.8f, 0.1f, 0.1f, 3.f } },
	{ { 1.5f,hy + (-zz * handscale),0 },{ 0.f, 0.f, 1.f },{ 0.8f, 0.1f, 0.1f, 3.f } },//rightarmshaft
	{ { -hx,hy,0 },{ 0.f, 0.f, 1.f },{ 1.f, 0.f, 0.f, 2.f } },//leftarmbase
	{ { -hx * (sms)-(2 * hy),hy + (handscale*z),x*handscale },{ 0.f, 0.f, 1.f },{ 1.f, 0.f, 0.f, 2.f } } ,
	{ { -hx * (sms)-(2 * hy),hy + (handscale*z),-x * handscale },{ 0.f, 0.f, 1.f },{ 1.f, 0.f, 0.f, 2.f } },
	{ { -hx * (sms)-(2 * hy),hy - (handscale*z),x*handscale },{ 0.f, 0.f, 1.f },{ 1.f, 0.f, 0.f, 2.f } },
	{ { -hx * (sms)-(2 * hy),hy - (handscale*z),-x * handscale },{ 0.f, 0.f, 1.f },{ 1.f, 0.f, 0.f, 2.f } },
	{ { -hx * (sms)-(2 * hy),hy + (zz*handscale),0 },{ 0.f, 0.f, 1.f },{ 1.f, 0.f, 0.f, 2.f } },
	{ { -hx * (sms)-(2 * hy),hy + (-zz * handscale),0 },{ 0.f, 0.f, 1.f },{ 1.f, 0.f, 0.f, 2.f } },//leftarmbase
	{ { -1.5f,hy + (handscale*z),x*handscale },{ 0.f, 0.f, 1.f },{ 1.f, 0.0f, 0.0f, 2.f } } ,//leftrightarmshaft
	{ { -1.5f,hy + (handscale*z),-x * handscale },{ 0.f, 0.f, 1.f },{ 1.f, 0.0f, 0.0f, 2.f } },
	{ { -1.5f,hy - (handscale*z),x*handscale },{ 0.f, 0.f, 1.f },{ 1.f, 0.0, 0.0f, 2.f} },
	{ { -1.5f,hy - (handscale*z),-x * handscale },{ 0.f, 0.f, 1.f },{ 1.f, 0.0f, 0.f, 2.f } },
	{ { -1.5f,hy + (zz*handscale),0 },{ 0.f, 0.f, 1.f },{ 1.f, 0.0f, 0.0f, 2.f } },
	{ { -1.5f,hy + (-zz * handscale),0 },{ 0.f, 0.f, 1.f },{ 1.f, 0.0f, 0.0f, 2.f } },//leftarmshaft
	{ { 1.75f,hy,0 },{ 0.f, 0.f, 1.f },{ 0.8f, 0.1f, 0.1f, 1.f } },//rightarmtip
	};

	static const uint16_t idxi[][3] =
	{
		{ 0,1,5 },//tip
	{ 0,1,3 },
	{ 0,2,5 },
	{ 0,2,4 },
	{ 0,4,6 },
	{ 0,3,6 },//tip
	{ 1,7,9 },//bot
	{ 1,9,3 },
	{ 5,1,11 },
	{ 11,7,1 },
	{ 5,11,8 },
	{ 8,2,5 },
	{ 2,8,4 },
	{ 4,10,8 },
	{ 4,6,12 },
	{ 4,10,12 },
	{ 6,12,3 },
	{ 3,9,12 },//bot
	{ 1 + o,7 + o,9 + o },//decline
	{ 1 + o,9 + o,3 + o },
	{ 5 + o,1 + o,11 + o },
	{ 11 + o,7 + o,1 + o },
	{ 5 + o,11 + o,8 + o },
	{ 8 + o,2 + o,5 + o },
	{ 2 + o,8 + o,4 + o },
	{ 4 + o,10 + o,8 + o },
	{ 4 + o,6 + o,12 + o },
	{ 4 + o,10 + o,12 + o },
	{ 6 + o,12 + o,3 + o },
	{ 3 + o,9 + o,12 + o },//decline
	{ 2 + (2 * o),5 + (2 * o),1 + (2 * o) },
	{ 4 + (2 * o),3 + (2 * o),6 + (2 * o) },//bottombase
	{ 4 + (2 * o),3 + (2 * o),2 + (2 * o) },
	{ 2 + (2 * o),3 + (2 * o),1 + (2 * o) },//bottombase
	{ 4 + (3 * o),3 + (3 * o),6 + (3 * o) },//rightlegdisk
	{ 4 + (3 * o),3 + (3 * o),2 + (3 * o) },
	{ 2 + (3 * o),5 + (3 * o),1 + (3 * o) },
	{ 2 + (3 * o),3 + (3 * o),1 + (3 * o) },//rightlegdisk

	{ 4 + (4 * o),3 + (4 * o),6 + (4 * o) },//leftlegdisk
	{ 4 + (4 * o),3 + (4 * o),2 + (4 * o) },
	{ 2 + (4 * o),5 + (4 * o),1 + (4 * o) },
	{ 2 + (4 * o),3 + (4 * o),1 + (4 * o) },//leftlegdisk
	{ 4 + (5 * o),3 + (5 * o),6 + (5 * o) },//rightlegdisk
	{ 4 + (5 * o),3 + (5 * o),2 + (5 * o) },
	{ 2 + (5 * o),5 + (5 * o),1 + (5 * o) },
	{ 2 + (5 * o),3 + (5 * o),1 + (5 * o) },//rightlegdisk

	{ 4 + (6 * o),3 + (6 * o),6 + (6 * o) },//leftlegdisk
	{ 4 + (6 * o),3 + (6 * o),2 + (6 * o) },
	{ 2 + (6 * o),5 + (6 * o),1 + (6 * o) },
	{ 2 + (6 * o),3 + (6 * o),1 + (6 * o) },//leftlegdisk
	{ 1 + (2 * o),7 + (2 * o),9 + (2 * o) },//it works i guess
	{ 1 + (2 * o),9 + (2 * o),3 + (2 * o) },
	{ 5 + (2 * o),1 + (2 * o),11 + (2 * o) },
	{ 11 + (2 * o),7 + (2 * o),1 + (2 * o) },
	{ 5 + (2 * o),11 + (2 * o),8 + (2 * o) },
	{ 8 + (2 * o),2 + (2 * o),5 + (2 * o) },
	{ 2 + (2 * o),8 + (2 * o),4 + (2 * o) },
	{ 4 + (2 * o),10 + (2 * o),8 + (2 * o) },
	{ 4 + (2 * o),6 + (2 * o),12 + (2 * o) },
	{ 4 + (2 * o),10 + (2 * o),12 + (2 * o) },
	{ 6 + (2 * o),12 + (2 * o),3 + (2 * o) },
	{ 3 + (3 * o),9 + (3 * o),12 + (3 * o) },//it works i guess
	{ 1 + (3 * o),7 + (3 * o),9 + (3 * o) },//rightlegdisk
	{ 1 + (3 * o),9 + (3 * o),3 + (3 * o) },
	{ 5 + (3 * o),1 + (3 * o),11 + (3 * o) },
	{ 11 + (3 * o),7 + (3 * o),1 + (3 * o) },
	{ 5 + (3 * o),11 + (3 * o),8 + (3 * o) },
	{ 8 + (3 * o),2 + (3 * o),5 + (3 * o) },
	{ 2 + (3 * o),8 + (3 * o),4 + (3 * o) },
	{ 4 + (3 * o),10 + (3 * o),8 + (3 * o) },
	{ 4 + (3 * o),6 + (3 * o),12 + (3 * o) },
	{ 4 + (3 * o),10 + (3 * o),12 + (3 * o) },
	{ 6 + (3 * o),12 + (3 * o),3 + (3 * o) },
	{ 3 + (3 * o),9 + (3 * o),12 + (3 * o) },//rightlegdisk
	{ 1 + (5 * o),7 + (5 * o),9 + (5 * o) },//leftlegdisk
	{ 1 + (5 * o),9 + (5 * o),3 + (5 * o) },
	{ 5 + (5 * o),1 + (5 * o),11 + (5 * o) },
	{ 11 + (5 * o),7 + (5 * o),1 + (5 * o) },
	{ 5 + (5 * o),11 + (5 * o),8 + (5 * o) },
	{ 8 + (5 * o),2 + (5 * o),5 + (5 * o) },
	{ 2 + (5 * o),8 + (5 * o),4 + (5 * o) },
	{ 4 + (5 * o),10 + (5 * o),8 + (5 * o) },
	{ 4 + (5 * o),6 + (5 * o),12 + (5 * o) },
	{ 4 + (5 * o),10 + (5 * o),12 + (5 * o) },
	{ 6 + (5 * o),12 + (5 * o),3 + (5 * o) },
	{ 3 + (5 * o),9 + (5 * o),12 + (5 * o) },//leftlegdisk
	{ 0 + (7 * o) + 1,1 + (7 * o) + 1,5 + (7 * o) + 1 },//rightlegtip
	{ 0 + (7 * o) + 1,1 + (7 * o) + 1,3 + (7 * o) + 1 },
	{ 0 + (7 * o) + 1,2 + (7 * o) + 1,5 + (7 * o) + 1 },
	{ 0 + (7 * o) + 1,2 + (7 * o) + 1,4 + (7 * o) + 1 },
	{ 0 + (7 * o) + 1,4 + (7 * o) + 1,6 + (7 * o) + 1 },
	{ 0 + (7 * o) + 1,3 + (7 * o) + 1,6 + (7 * o) + 1 },//rightlegtip
	{ 0 + (8 * o) + 2,1 + (8 * o) + 2,5 + (8 * o) + 2 },//leftlegtip
	{ 0 + (8 * o) + 2,1 + (8 * o) + 2,3 + (8 * o) + 2 },
	{ 0 + (8 * o) + 2,2 + (8 * o) + 2,5 + (8 * o) + 2 },
	{ 0 + (8 * o) + 2,2 + (8 * o) + 2,4 + (8 * o) + 2 },
	{ 0 + (8 * o) + 2,4 + (8 * o) + 2,6 + (8 * o) + 2 },
	{ 0 + (8 * o) + 2,3 + (8 * o) + 2,6 + (8 * o) + 2 },//leftlegtip
	{ 0 + (9 * o) + 3,1 + (9 * o) + 3,5 + (9 * o) + 3 },//righthandbase
	{ 0 + (9 * o) + 3,1 + (9 * o) + 3,3 + (9 * o) + 3 },
	{ 0 + (9 * o) + 3,2 + (9 * o) + 3,5 + (9 * o) + 3 },
	{ 0 + (9 * o) + 3,2 + (9 * o) + 3,4 + (9 * o) + 3 },
	{ 0 + (9 * o) + 3,4 + (9 * o) + 3,6 + (9 * o) + 3 },
	{ 0 + (9 * o) + 3,3 + (9 * o) + 3,6 + (9 * o) + 3 },//righthandbase
	{ 1 + (9 * o) + 3,7 + (9 * o) + 3,9 + (9 * o) + 3 },//righthanddisk
	{ 1 + (9 * o) + 3,9 + (9 * o) + 3,3 + (9 * o) + 3 },
	{ 5 + (9 * o) + 3,1 + (9 * o) + 3,11 + (9 * o) + 3 },
	{ 11 + (9 * o) + 3,7 + (9 * o) + 3,1 + (9 * o) + 3 },
	{ 5 + (9 * o) + 3,11 + (9 * o) + 3,8 + (9 * o) + 3 },
	{ 8 + (9 * o) + 3,2 + (9 * o) + 3,5 + (9 * o) + 3 },
	{ 2 + (9 * o) + 3,8 + (9 * o) + 3,4 + (9 * o) + 3 },
	{ 4 + (9 * o) + 3,10 + (9 * o) + 3,8 + (9 * o) + 3 },
	{ 4 + (9 * o) + 3,6 + (9 * o) + 3,12 + (9 * o) + 3 },
	{ 4 + (9 * o) + 3,10 + (9 * o) + 3,12 + (9 * o) + 3 },
	{ 6 + (9 * o) + 3,12 + (9 * o) + 3,3 + (9 * o) + 3 },
	{ 3 + (9 * o) + 3,9 + (9 * o) + 3,12 + (9 * o) + 3 },//righthanddisk
	{ 0 + (11 * o) + 4,1 + (11 * o) + 4,5 + (11 * o) + 4 },//lefthandbase
	{ 0 + (11 * o) + 4,1 + (11 * o) + 4,3 + (11 * o) + 4 },
	{ 0 + (11 * o) + 4,2 + (11 * o) + 4,5 + (11 * o) + 4 },
	{ 0 + (11 * o) + 4,2 + (11 * o) + 4,4 + (11 * o) + 4 },
	{ 0 + (11 * o) + 4,4 + (11 * o) + 4,6 + (11 * o) + 4 },
	{ 0 + (11 * o) + 4,3 + (11 * o) + 4,6 + (11 * o) + 4 },//lefthandbase
	{ 1 + (11 * o) + 4,7 + (11 * o) + 4,9 + (11 * o) + 4 },//lefthanddisk
	{ 1 + (11 * o) + 4,9 + (11 * o) + 4,3 + (11 * o) + 4 },
	{ 5 + (11 * o) + 4,1 + (11 * o) + 4,11 + (11 * o) + 4 },
	{ 11 + (11 * o) + 4,7 + (11 * o) + 4,1 + (11 * o) + 4 },
	{ 5 + (11 * o) + 4,11 + (11 * o) + 4,8 + (11 * o) + 4 },
	{ 8 + (11 * o) + 4,2 + (11 * o) + 4,5 + (11 * o) + 4 },
	{ 2 + (11 * o) + 4,8 + (11 * o) + 4,4 + (11 * o) + 4 },
	{ 4 + (11 * o) + 4,10 + (11 * o) + 4,8 + (11 * o) + 4 },
	{ 4 + (11 * o) + 4,6 + (11 * o) + 4,12 + (11 * o) + 4 },
	{ 4 + (11 * o) + 4,10 + (11 * o) + 4,12 + (11 * o) + 4 },
	{ 6 + (11 * o) + 4,12 + (11 * o) + 4,3 + (11 * o) + 4 },
	{ 3 + (11 * o) + 4,9 + (11 * o) + 4,12 + (11 * o) + 4 },//lefthanddisk
	{ 83 + 1,1 + (10 * o) + 3,5 + (10 * o) + 3 },//righthandtip
	{ 83 + 1,1 + (10 * o) + 3,3 + (10 * o) + 3 },
	{ 83 + 1,2 + (10 * o) +3,5 + (10 * o) + 3 },
	{ 83 + 1,2 + (10 * o) + 3,4 + (10 * o) + 3 },
	{ 83 + 1,4 + (10 * o) + 3,6 + (10 * o) + 3 },
	{ 83 + 1,3 + (10 * o) + 3,6 + (10 * o) + 3 },//righthandtip
	};

	g_num_faces[MESH_SKIN] = sizeof(idxi) / sizeof(idxi[0]);

	const unsigned num_verts = sizeof(arri) / sizeof(arri[0]);
	const unsigned num_indes = sizeof(idxi) / sizeof(idxi[0][0]);

	std::cout << "number of vertices: " << num_verts <<
		"\nnumber of indices: " << num_indes << std::endl;

	/////////////////////////////////////////////////////////////////
	// reserve VAO (if available) and all necessary VBOs

#if PLATFORM_GL_OES_vertex_array_object
	glGenVertexArraysOES(sizeof(g_vao) / sizeof(g_vao[0]), g_vao);

	for (unsigned i = 0; i < sizeof(g_vao) / sizeof(g_vao[0]); ++i)
		assert(g_vao[i]);

	glBindVertexArrayOES(g_vao[PROG_SKIN]);

#endif
	glGenBuffers(sizeof(g_vbo) / sizeof(g_vbo[0]), g_vbo);

	for (unsigned i = 0; i < sizeof(g_vbo) / sizeof(g_vbo[0]); ++i)
		assert(g_vbo[i]);

	/////////////////////////////////////////////////////////////////
	// bind VBOs and upload the geom asset
	
	glBindBuffer(GL_ARRAY_BUFFER, g_vbo[VBO_SKIN_VTX]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(arri), arri, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, g_vbo[VBO_SKIN_IDX]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(idxi), idxi, GL_STATIC_DRAW);

	
	DEBUG_GL_ERR()

	DEBUG_GL_ERR()

	/////////////////////////////////////////////////////////////////
	// set up the vertex attrib mapping for all attrib inputs

	if (!setupVertexAttrPointers< Vertex >(g_active_attr_semantics[PROG_SKIN])) {
		std::cerr << __FUNCTION__ << " failed at setupVertexAttrPointers" << std::endl;
		return false;
	}

#if PLATFORM_GL_OES_vertex_array_object
	/////////////////////////////////////////////////////////////////
	// if VAO: the final step of enabling the mapped attrib inputs

	for (unsigned i = 0; i < g_active_attr_semantics[PROG_SKIN].num_active_attr; ++i)
		glEnableVertexAttribArray(g_active_attr_semantics[PROG_SKIN].active_attr[i]);

	DEBUG_GL_ERR()

	glBindVertexArrayOES(0);

#endif
	/////////////////////////////////////////////////////////////////
	// unbind the VBOs -- will be re-bound on a need-to-use basis

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	on_error.reset();
	return true;
}

namespace { // anonymous

class matx4_ortho : public simd::matx4
{
	matx4_ortho();
	matx4_ortho(const matx4_ortho&);

public:
	matx4_ortho(
		const float l,
		const float r,
		const float b,
		const float t,
		const float n,
		const float f)
	{
		static_cast< simd::matx4& >(*this) = simd::matx4(
			2.f / (r - l),     0.f,               0.f,               0.f,
			0.f,               2.f / (t - b),     0.f,               0.f,
			0.f,               0.f,               2.f / (n - f),     0.f,
			(r + l) / (l - r), (t + b) / (b - t), (f + n) / (n - f), 1.f);
	}
};


class matx4_persp : public simd::matx4
{
	matx4_persp();
	matx4_persp(const matx4_persp&);

public:
	matx4_persp(
		const float l,
		const float r,
		const float b,
		const float t,
		const float n,
		const float f)
	{
		static_cast< simd::matx4& >(*this) = simd::matx4(
			2.f * n / (r - l),  0.f,                0.f,                    0.f,
			0.f,                2.f * n / (t - b),  0.f,                    0.f,
			(r + l) / (r - l),  (t + b) / (t - b),  (f + n) / (n - f),     -1.f,
			0.f,                0.f,                2.f * f * n / (n - f),  0.f);
	}
};

class matx3;
matx3 matx3_mul(const matx3&, const matx3&);

class matx3 {
	friend matx3 matx3_mul(const matx3&, const matx3&);
	float m[3][3];

public:
	matx3() {}

	matx3(
		const float c0_0, const float c0_1, const float c0_2,
		const float c1_0, const float c1_1, const float c1_2,
		const float c2_0, const float c2_1, const float c2_2)
	{
		m[0][0] = c0_0;
		m[0][1] = c0_1;
		m[0][2] = c0_2;
		m[1][0] = c1_0;
		m[1][1] = c1_1;
		m[1][2] = c1_2;
		m[2][0] = c2_0;
		m[2][1] = c2_1;
		m[2][2] = c2_2;
	}

	const float (& operator[](const size_t i) const)[3] { return m[i]; }

	template < size_t NUM_VECT, size_t NUM_ELEM >
	void transform(
		const float (& a)[NUM_VECT][NUM_ELEM],
		float (& out)[NUM_VECT][NUM_ELEM]) const {

		const size_t num_vect = NUM_VECT;
		const size_t num_elem = 3;
		const util::compile_assert< num_elem <= NUM_ELEM > assert_num_elem;

		for (size_t j = 0; j < num_vect; ++j) {
			float out_j[num_elem];
			const float a_j0 = a[j][0];
			for (size_t i = 0; i < num_elem; ++i) {
				out_j[i] = a_j0 * m[0][i];
			}
			for (size_t k = 1; k < num_elem; ++k) {
				const float a_jk = a[j][k];
				for (size_t i = 0; i < num_elem; ++i) {
					out_j[i] += a_jk * m[k][i];
				}
			}
			for (size_t i = 0; i < num_elem; ++i)
				out[j][i] = out_j[i];
			for (size_t i = num_elem; i < NUM_ELEM; ++i)
				out[j][i] = a[j][i];
		}
	}
};

matx3 matx3_mul(
	const matx3& ma,
	const matx3& mb)
{
	matx3 mc;
	mb.transform(ma.m, mc.m);
	return mc;
}

matx3 matx3_rotate(
	const float a,
	const float x,
	const float y,
	const float z)
{
	const float sin_a = sinf(a);
	const float cos_a = cosf(a);

	return matx3(
		/*x * x + cos_a * (1 - x * x),         x * y - cos_a * (x * y) + sin_a * z, x * z - cos_a * (x * z) - sin_a * y,
		y * x - cos_a * (y * x) - sin_a * z, y * y + cos_a * (1 - y * y),         y * z - cos_a * (y * z) + sin_a * x,
		z * x - cos_a * (z * x) + sin_a * y, z * y - cos_a * (z * y) - sin_a * x, z * z + cos_a * (1 - z * z)*/0.75f,0,0,0,0.75f,0,0,0,0.75f);
}

} // namespace


int renderFrame()
{
	if (!check_context(__FUNCTION__))
		return false;

	/////////////////////////////////////////////////////////////////
	// clear the framebuffer (both color and depth)

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	/////////////////////////////////////////////////////////////////
	// query about the viewport geometry; used for aspect

	GLint vp[4];
	glGetIntegerv(GL_VIEWPORT, vp);

	/////////////////////////////////////////////////////////////////
	// animate the skeleton for the given timestamp

	static float anim = 0.f;

	rend::animateSkeleton(bone_count, g_bone_mat, g_bone, g_skeletal_animation, anim);

	anim += g_anim_step;

	if (1.f < anim) {
		anim -= floorf(anim);
		rend::resetSkeletonAnimProgress(g_skeletal_animation);
	}

	/////////////////////////////////////////////////////////////////
	// produce mvp matrix

	// basis vectors
	const matx3 r0 = matx3_rotate(M_PI / 4, 0.f, 1.f, 0.f);
	const matx3 r1 = matx3_rotate(M_PI / 4, 1.f, 0.f, 0.f);
	const matx3 combined = matx3_mul(r0, r1);

	const simd::matx4 mv = simd::matx4(
		combined[0][0],  combined[0][1],  combined[0][2],  0.f,
		combined[1][0],  combined[1][1],  combined[1][2],  0.f,
		combined[2][0],  combined[2][1],  combined[2][2],  0.f,
		0.f,             0.f,            -2.f,             1.f);

	const float aspect = float(vp[2]) / vp[3];
	const float l = aspect * -.5f;
	const float r = aspect * .5f;
	const float b = -.5f;
	const float t = .5f;
	const float n = 1.f;
	const float f = 4.f;

	const matx4_persp proj(l, r, b, t, n, f);

	const simd::matx4 mvp = simd::matx4().mul(mv, proj);
	const rend::dense_matx4 dense_mvp = rend::dense_matx4(
			mvp[0][0], mvp[0][1], mvp[0][2], mvp[0][3],
			mvp[1][0], mvp[1][1], mvp[1][2], mvp[1][3],
			mvp[2][0], mvp[2][1], mvp[2][2], mvp[2][3],
			mvp[3][0], mvp[3][1], mvp[3][2], mvp[3][3]);

	/////////////////////////////////////////////////////////////////
	// activate the shader program and set up all valid uniform vars

	glUseProgram(g_shader_prog[PROG_SKIN]);

	DEBUG_GL_ERR()

	if (-1 != g_uni[PROG_SKIN][UNI_MVP])
	{
		glUniformMatrix4fv(g_uni[PROG_SKIN][UNI_MVP],
			1, GL_FALSE, static_cast< const GLfloat* >(dense_mvp));
	}

	DEBUG_GL_ERR()

	if (-1 != g_uni[PROG_SKIN][UNI_BONE])
	{
		glUniformMatrix4fv(g_uni[PROG_SKIN][UNI_BONE],
			bone_count, GL_FALSE, static_cast< const GLfloat* >(g_bone_mat[0]));
	}

	DEBUG_GL_ERR()

	if (-1 != g_uni[PROG_SKIN][UNI_LP_OBJ])
	{
		const GLfloat nonlocal_light[4] =
		{
			mv[0][2],
			mv[1][2],
			mv[2][2],
			0.f
		};

		glUniform4fv(g_uni[PROG_SKIN][UNI_LP_OBJ], 1, nonlocal_light);
	}

	DEBUG_GL_ERR()

	if (-1 != g_uni[PROG_SKIN][UNI_VP_OBJ])
	{
		const GLfloat nonlocal_viewer[4] =
		{
			mv[0][2],
			mv[1][2],
			mv[2][2],
			0.f
		};

		glUniform4fv(g_uni[PROG_SKIN][UNI_VP_OBJ], 1, nonlocal_viewer);
	}

	DEBUG_GL_ERR()

	if (0 != g_tex[TEX_NORMAL] && -1 != g_uni[PROG_SKIN][UNI_SAMPLER_NORMAL])
	{
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, g_tex[TEX_NORMAL]);

		glUniform1i(g_uni[PROG_SKIN][UNI_SAMPLER_NORMAL], 0);
	}

	DEBUG_GL_ERR()

	if (0 != g_tex[TEX_ALBEDO] && -1 != g_uni[PROG_SKIN][UNI_SAMPLER_ALBEDO])
	{
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, g_tex[TEX_ALBEDO]);

		glUniform1i(g_uni[PROG_SKIN][UNI_SAMPLER_ALBEDO], 1);
	}

	DEBUG_GL_ERR()

#if PLATFORM_GL_OES_vertex_array_object
	glBindVertexArrayOES(g_vao[PROG_SKIN]);

#else
	/////////////////////////////////////////////////////////////////
	// no VAO: re-bind the VBOs and enable all mapped vertex attribs

	glBindBuffer(GL_ARRAY_BUFFER, g_vbo[VBO_SKIN_VTX]);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, g_vbo[VBO_SKIN_IDX]);

	for (unsigned i = 0; i < g_active_attr_semantics[PROG_SKIN].num_active_attr; ++i)
		glEnableVertexAttribArray(g_active_attr_semantics[PROG_SKIN].active_attr[i]);

	DEBUG_GL_ERR()

#endif
	/////////////////////////////////////////////////////////////////
	// draw the geometry asset

	glDrawElements(GL_TRIANGLES, g_num_faces[MESH_SKIN] * 3, GL_UNSIGNED_SHORT, (void*) 0);

	DEBUG_GL_ERR()

#if PLATFORM_GL_OES_vertex_array_object == 0
	/////////////////////////////////////////////////////////////////
	// no VAO: disable all mapped vertex attribs and unbind VBOs

	for (unsigned i = 0; i < g_active_attr_semantics[PROG_SKIN].num_active_attr; ++i)
		glDisableVertexAttribArray(g_active_attr_semantics[PROG_SKIN].active_attr[i]);

	DEBUG_GL_ERR()

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

#endif
	return true;
}
